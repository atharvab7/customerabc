public with sharing class CustomSenderService implements Callable {
    private static final String senderQuery = 'Select Id, OwnerId, Owner.Name, smagicinteract__senderId__c,smagicinteract__Description__c, smagicinteract__Enable_Email_To_Text__c, smagicinteract__Configured_Manage_Notification__c, '
        + 'smagicinteract__Used_For__c,smagicinteract__Email_Notification_Template__c, smagicinteract__Reply_To_Address__c, '
        + 'smagicinteract__Notification_Recipient__c,smagicinteract__Label__c, smagicinteract__Send_Notification_From_Email__c, smagicinteract__Channel_Code__c, smagicinteract__Channel_Name__c,'
        + ' (SELECT Id, smagicinteract__Profile__c, smagicinteract__Profile_Id__c,'
        + 'smagicinteract__SenderId_Lookup__c, smagicinteract__Sender_Id__c,'
        + ' smagicinteract__User__c, smagicinteract__User__r.Name'
        + ' FROM smagicinteract__SenderId_Profile_Map__r'
        + ' ORDER BY smagicinteract__User__c ASC NULLS LAST LIMIT 999),'
        + ' (SELECT Id, Name, smagicinteract__Incoming_Sender_ID__r.smagicinteract__senderId__c,'
        + 'smagicinteract__Incoming_Sender_ID__r.smagicinteract__Label__c'
        + ' from smagicinteract__Outgoing_Alpha_SenderID_Map__r)'
        + ' from smagicinteract__SMS_SenderId__c';

    @TestVisible
    private enum Criteria {
        All, By_Ids, By_Name, Current_User, DefaultCriteria, Automation
    }

    public Object call(String action, Map<String, Object> args) {
        switch on action {
            when 'getSenderInfo' {
                return this.getSenderIds(
                    (RequestContext) JSON.deserialize(
                        String.valueOf(args.get('requestContext')),
                        RequestContext.class
                    ),
                    Criteria.valueOf(String.valueOf(args.get('givenCriteria'))),
                    (List<String>) args.get('ids')
                );
            }
            when else {
                throw new SenderServiceMalformedCallException('Method not implemented');
            }
        }
    }

    private String getSenderIds(RequestContext requestContext, Criteria givenCriteria, List<String> ids) {
        String senderIdFromContact;
        String query;
        String senderId;
        Map<Id, String> defaultSenderIdMap = getDefaultSenderIdMap(
            new Set<Id>{
                UserInfo.getUserId()
            }
        );
        senderId = defaultSenderIdMap.get(UserInfo.getUserId());

        if (givenCriteria == Criteria.DefaultCriteria || givenCriteria == Criteria.All) {
            senderIdFromContact = getSenderIdFromContact(givenCriteria, ids);
            givenCriteria = Criteria.All;
            query = senderQuery + ' order by smagicinteract__senderId__c LIMIT 999';
        } else if (givenCriteria == Criteria.By_Ids && ids != null) {
            query = senderQuery + ' where id in :ids order by smagicinteract__senderId__c LIMIT 999';
        } else if (givenCriteria == Criteria.By_Name && ids != null) {
            query = senderQuery + ' where smagicinteract__senderId__c in :ids order by smagicinteract__senderId__c LIMIT 999';
        } else if (givenCriteria == Criteria.Current_User) {
            query = senderQuery + ' where smagicinteract__senderId__c = :senderId order by smagicinteract__senderId__c';
        } else {
            throw new SenderServiceMalformedCallException('Invalid input');
        }

        List<smagicinteract__SMS_SenderId__c> senderList = (List<smagicinteract__SMS_SenderId__c>) Database.query(query);

        List<SenderInfo> senderInfos = transformData(senderList);

        if (String.isNotBlank(senderId) || String.isNotBlank(senderIdFromContact)) {
            String senderIdForCheck = String.isNotBlank(senderIdFromContact) ? senderIdFromContact : senderId;
            for (SenderInfo info : senderInfos) {
                if (info.senderId.equalsIgnoreCase(senderIdForCheck)) {
                    info.isDefault = true;
                }
            }
        }

        if (requestContext != null) {
            senderInfos = evluateRequestContext(requestContext, givenCriteria, senderInfos);
        }

        return JSON.serialize(senderInfos);
    }

    private String getSenderIdFromContact(Criteria givenCriteria, List<String> ids) {
        String senderIdFromContact;

        if (givenCriteria == Criteria.DefaultCriteria && ids != null && ids.size() == 1) {
            Id recordId = (Id) ids[0];

            if (recordId.getSobjectType() == Contact.getSObjectType()) {
                List<Contact> contacts = [SELECT Id, phone FROM Contact WHERE Id = :recordId AND phone != NULL];
                if(!contacts.isEmpty()){
                   senderIdFromContact = contacts[0].phone.replaceAll('[\\D]', '');
                }
            }
        }

        return senderIdFromContact;
    }

    private static Map<Id, String> getDefaultSenderIdMap(Set<Id> userIdList) {
        Map<Id, String> userDefaultSenderIdMap = new Map<Id, String>();
        if (userIdList == null || userIdList.isEmpty())
            return userDefaultSenderIdMap;

        //Maintains a mapping of userId and its profileId
        Map<Id, Id> usrProfileMap = new Map<Id, Id>();
        Set<Id> profileIdSet = new Set<Id>();
        String profileFetchQuery = 'select Id, ProfileId from User where Id IN :userIdList';
        List<User> users = Database.query(profileFetchQuery);

        for (User user : users) {
            usrProfileMap.put(user.Id, user.ProfileId);
        }
        profileIdSet = new Set<Id>(usrProfileMap.values());
        //Fetch sender id based on userId or its profileId, Order by clause to first check senderId against userId then profileId
        String query = 'select id,smagicinteract__Default_SenderId__c,smagicinteract__User__c,smagicinteract__Profile_Id__c,' +
            'smagicinteract__SenderId_Lookup__r.smagicinteract__Channel_Code__c' +
            ' from smagicinteract__SenderId_Profile_Map__c where smagicinteract__User__c IN :userIdList or ' +
            'smagicinteract__Profile_Id__c IN :profileIdSet order by smagicinteract__User__c ASC NULLS last';

        List<smagicinteract__SenderId_Profile_Map__c> defaultSenderIdList = Database.query(query);


        //check if senderid based on userId, if not check entry based on profileId
        for (Id userId : userIdList) {
            String userProfileId = usrProfileMap.get(userId);

            if (defaultSenderIdList != null && defaultSenderIdList.size() > 0) {
                for (smagicinteract__SenderId_Profile_Map__c defaultSenderId : defaultSenderIdList) {
                    String tempUserID = defaultSenderId.smagicinteract__User__c;
                    String tempProfileId = defaultSenderId.smagicinteract__Profile_Id__c;
                    String senderId = defaultSenderId.smagicinteract__Default_SenderId__c;

                    //SenderId found based on userId or profileId,
                    if (userId == tempUserID || (userProfileId != null && userProfileId == tempProfileId)) {
                        //contains userid check
                        if (!userDefaultSenderIdMap.containsKey(userId)) {
                            userDefaultSenderIdMap.put(userId, senderId);
                            break;
                        }
                    }
                }
            }
        }

        return userDefaultSenderIdMap;
    }

    private static List<SenderInfo> transformData(List<smagicinteract__SMS_SenderId__c> senderList) {
        List<SenderInfo> tempList = new List<SenderInfo>();
        if (senderList.isEmpty()) {
            return tempList;
        }
        for (smagicinteract__SMS_SenderId__c record : senderList) {
            tempList.add(new SenderInfo(record));
        }
        tempList.sort();

        return tempList;
    }

    private static List<SenderInfo> evluateRequestContext(RequestContext requestContext, Criteria givenCriteria, List<SenderInfo> senderData) {
        List<SenderInfo> tempList = new List<SenderInfo> ();
        tempList.addAll(senderData);
        Set<String> originSet = new Set<String>{
            'DESK',
            'NEW_MESSAGE',
            'DESK_NEW_MESSAGE',
            'BULK_SMS',
            'CONVERSE_APP_TEMPLATE_SETUP',
            'ROUTING_CONFIG',
            'CONVERSE_APP_LISTVIEW',
            'CONVERSATION_VIEW',
            'QUICK_REPLY',
            'CONVERSATION_LIGHT',
            'CONVERSE_APP_AUTOMATION'
        };

        if (requestContext != null && requestContext.originContext != null &&
            requestContext.originContext.originName == 'CONVERSE_APP_AUTOMATION') {
            List<PicklistOption> senderIdCriteriaList = getPickList(
                'smagicinteract__Converse_App_Action__c',
                'smagicinteract__SenderIDSelectionCriteria__c'
            );

            for (PicklistOption senderIdCriteria : senderIdCriteriaList) {
                if (senderIdCriteria.name.equalsIgnoreCase('RECORD_OWNERS_SENDERID') || senderIdCriteria.name.equalsIgnoreCase('AUTO_SENDERID_VALUE')) {
                    tempList.add(new SenderInfo(senderIdCriteria.name, senderIdCriteria.label));
                }
            }

            for (SenderInfo info : tempList) {
                info.isDefault = false;
            }

        } else if (requestContext.originContext != null && originSet.contains(requestContext.originContext.originName)) {
// We deleted one conditions because we don't have access to custom settings
            tempList.add(new SenderInfo('Record Owner\'s SenderID', 'Record Owner\'s SenderId'));
        }

        return tempList;
    }

    private static List<PicklistOption> getPickList(String objectName, String FieldName) {
        if (String.isBlank(objectName)) {
            throw new SenderServiceMalformedCallException('Object name cannot be blank');
        }
        if (String.isBlank(fieldName)) {
            throw new SenderServiceMalformedCallException('Field name is blank.');
        }

        List<PicklistOption> pickList = new List<PicklistOption>();
        Schema.SObjectType objectDesc = Schema.getGlobalDescribe().get(objectName);

        if (objectDesc == null) {
            throw new SenderServiceMalformedCallException('Invalid Object Name.');
        }

        Schema.SObjectField fieldDesc = objectDesc.getDescribe().fields.getMap().get(FieldName);
        if (fieldDesc == null) {
            throw new SenderServiceMalformedCallException('Field name is invalid.');
        }

        Schema.DescribeFieldResult fieldResult = fieldDesc.getDescribe();
        List<Schema.PicklistEntry> pickListDesc = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry pickListField : pickListDesc) {
            pickList.add(new PicklistOption(pickListField.getValue(), pickListField.getLabel(), pickListField.getValue(), false));
        }

        return pickList;
    }

    @TestVisible
    private with sharing class SenderInfo implements Comparable {
        public smagicinteract__SMS_SenderId__c senderRecord { get; set; }
        public String id { get; set; }
        public String senderId { get; set; }
        public String label { get; set; }
        public String description { get; set; }
        public String used_for { get; set; }
        public String email_Notification_Template { get; set; }
        public String notification_Recipient { get; set; }
        public Boolean isDefault { get; set; }
        public Set<Id> assignedUsers { get; set; }
        public Boolean isSenderIdValid { get; set; }
        public String failureMessage { get; set; }
        public String channelCode { get; set; }

        public Boolean equals(Object obj) {
            if (obj instanceof SenderInfo && ((SenderInfo) obj).id == this.id && ((SenderInfo) obj).label == this.label)
                return true; else
                return false;
        }

        public Integer hashCode() {
            Integer result = 0;
            if (String.isNotBlank(senderId))
                result = 17 * senderId.hashCode();

            return id == null ? result : result + 17 * id.hashCode();
        }

        public Integer compareTo(Object otherObject) {
            SenderInfo otherOption = (SenderInfo) otherObject;
            return this.label > otherOption.label ? 1 : this.label < otherOption.label ? -1 : 0;
        }

        public SenderInfo(String senderId, String label) {
            this.senderId = senderId;
            this.label = label;
            this.assignedUsers = new Set<Id> ();
            this.isSenderIdValid = true;
            this.isDefault = false;
        }

        public SenderInfo(smagicinteract__SMS_SenderId__c record) {
            if (record == null) {
                this.isSenderIdValid = false;
                this.failureMessage = 'invalid_sender_id';
                return;
            }
            this.channelCode = String.isNotBlank(record.smagicinteract__Channel_Code__c) ? record.smagicinteract__Channel_Code__c : '1';
            this.isSenderIdValid = true;
            this.senderRecord = record;
            this.id = record.Id;
            this.senderId = record.smagicinteract__senderId__c;
            if (String.isNotBlank(record.smagicinteract__Label__c)) {
                this.label = record.smagicinteract__Label__c + ': ' + record.smagicinteract__senderId__c;
            } else {
                this.label = record.smagicinteract__senderId__c;
            }
            this.description = record.smagicinteract__Description__c;
            this.used_for = record.smagicinteract__Used_For__c;
            this.email_Notification_Template = record.smagicinteract__Email_Notification_Template__c;
            this.notification_Recipient = record.smagicinteract__Notification_Recipient__c;
            List<smagicinteract__SenderId_Profile_Map__c> senderMapList = record.smagicinteract__SenderId_Profile_Map__r;
            this.assignedUsers = new Set<Id> ();
            if (senderMapList != null && !senderMapList.isEmpty()) {
                for (smagicinteract__SenderId_Profile_Map__c rec : senderMapList) {
                    this.assignedUsers.add(rec.smagicinteract__Profile_Id__c);
                    this.assignedUsers.add(rec.smagicinteract__User__c);
                }
                this.assignedUsers.remove(null);
            }

            this.isDefault = false;
        }

    }

    @TestVisible
    private with sharing class PicklistOption implements Comparable {

        @AuraEnabled
        public String name { get; set; }
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String value { get; set; }
        @AuraEnabled
        public Boolean defaultSelected { get; set; }
        @AuraEnabled
        public String channelLabel { get; set; }

        public PicklistOption(String name, String label, String value, Boolean defaultSelected) {
            this.name = name ;
            this.label = label;
            this.value = value;
            this.defaultSelected = defaultSelected;
        }

        public PicklistOption(String name, String label, String value, Boolean defaultSelected, String channelLabel) {
            this.name = name ;
            this.label = label;
            this.value = value;
            this.defaultSelected = defaultSelected;
            this.channelLabel = channelLabel;
        }

        public Integer compareTo(Object obj) {
            PicklistOption otherObj = (PicklistOption) obj;
            Integer retValue = 0;
            if (this.channelLabel != null && otherObj.channelLabel != null) {
                retValue = (this.channelLabel > otherObj.channelLabel)
                    ? 1
                    : ((this.channelLabel < otherObj.channelLabel)
                        ? -1 : 0);
                if (retValue == 0) {
                    retValue = (this.name > otherObj.name)
                        ? 1
                        : ((this.name < otherObj.name)
                            ? -1 : 0);
                }
            } else {
                retValue = (this.label > otherObj.label) ? 1 : (this.label < otherObj.label ? -1 : 0);
            }
            return retValue;
        }
    }

    @TestVisible
    private class RequestContext {
        public OriginContext originContext { get; set; }
        public String featureContext { get; set; }
    }

    @TestVisible
    private with sharing class OriginContext {
        public String originId { get; set; }
        public String originName { get; set; }
        public String originSourceCode { get; set; }

    }

    public class SenderServiceMalformedCallException extends Exception {
    }
}