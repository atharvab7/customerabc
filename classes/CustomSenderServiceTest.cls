@IsTest
private class CustomSenderServiceTest {
    private static final String CALL_CENTER_PHONE = '7204358081';
    private static final String TEST_REQUEST_CONTEXT = '{"viewContext":{"view":"LIGHTNING"},"originContext":{"originSourceCode":null,"originName":"CONVERSATION_VIEW","originId":"","embeddedContext":{"location":"TAB"}},"messageContext":{"scheduleContext":null,"objectApiName":null,"messageChannel":null},"featureContext":null}';

    @TestSetup
    static void makeData() {
        Account account = new Account(
            Name = 'Test Account',
            phone = CALL_CENTER_PHONE
        );
        insert account;

        Contact contact = new Contact(
            FirstName = 'Test1',
            LastName = 'One',
            Email = 'rj@one.com',
            AccountId = account.Id,
            phone = '11111111111'
        );
        insert contact;
    }

    @IsTest
    static void callTestWithOutData() {
        Contact contact = [SELECT Id FROM Contact WHERE Email = 'rj@one.com' LIMIT 1];
        Map<String, Object> parameterMap = new Map<String, Object>();
        parameterMap.put('requestContext', TEST_REQUEST_CONTEXT);
        parameterMap.put('givenCriteria', String.valueOf(CustomSenderService.Criteria.DefaultCriteria));
        parameterMap.put('ids', new List<String>{
            contact.Id
        });

        List<CustomSenderService.SenderInfo> senderInfos = (List<CustomSenderService.SenderInfo>) JSON.deserialize(
            (String) new CustomSenderService().call('getSenderInfo', parameterMap),
            List<CustomSenderService.SenderInfo>.class
        );
        System.debug('senderInfos : ' + senderInfos);

        System.assertEquals(1, senderInfos.size(), 'We should have a default record');
    }

    @IsTest
    static void callTestWithData() {
        Contact contact = [SELECT Id FROM Contact WHERE Email = 'rj@one.com' LIMIT 1];
        Map<String, Object> parameterMap = new Map<String, Object>();
        parameterMap.put('requestContext', TEST_REQUEST_CONTEXT);
        parameterMap.put('givenCriteria', String.valueOf(CustomSenderService.Criteria.DefaultCriteria));
        parameterMap.put('ids', new List<String>{
            contact.Id
        });

        smagicinteract__SMS_SenderId__c senderId = getNewSenderId();
        smagicinteract__SMS_SenderId__c senderIdFromContact = getNewSenderId();
        senderIdFromContact.smagicinteract__senderId__c = CALL_CENTER_PHONE;

        insert new List<smagicinteract__SMS_SenderId__c>{
            senderId,
            senderIdFromContact
        };

        List<CustomSenderService.SenderInfo> senderInfos = (List<CustomSenderService.SenderInfo>) JSON.deserialize(
            (String) new CustomSenderService().call('getSenderInfo', parameterMap),
            List<CustomSenderService.SenderInfo>.class
        );

        System.assertEquals(3, senderInfos.size());
        System.debug('senderInfos; ' + senderInfos);
        for (CustomSenderService.SenderInfo senderInfo : senderInfos) {
            if (senderInfo.senderRecord != null && senderInfo.senderRecord.Id == senderIdFromContact.Id) {
                //System.assert(senderInfo.isDefault);
            } else {
                //System.assert(!senderInfo.isDefault);
            }
        }
    }

    @IsTest
    static void compereToTest() {
        Contact contact = [SELECT Id FROM Contact WHERE Email = 'rj@one.com' LIMIT 1];
        Map<String, Object> parameterMap = new Map<String, Object>();
        parameterMap.put('requestContext', TEST_REQUEST_CONTEXT.replace('CONVERSATION_VIEW', 'CONVERSE_APP_AUTOMATION'));
        parameterMap.put('givenCriteria', String.valueOf(CustomSenderService.Criteria.DefaultCriteria));
        parameterMap.put('ids', new List<String>{
            contact.Id
        });

        smagicinteract__SMS_SenderId__c senderId = getNewSenderId();
        smagicinteract__SMS_SenderId__c senderIdFromContact = getNewSenderId();
        senderIdFromContact.smagicinteract__senderId__c = CALL_CENTER_PHONE;

        insert new List<smagicinteract__SMS_SenderId__c>{
            senderId,
            senderIdFromContact
        };

        List<CustomSenderService.SenderInfo> senderInfos = (List<CustomSenderService.SenderInfo>) JSON.deserialize(
            (String) new CustomSenderService().call('getSenderInfo', parameterMap),
            List<CustomSenderService.SenderInfo>.class
        );

        System.assertNotEquals(senderInfos[0], senderInfos[1]);

        List<CustomSenderService.PicklistOption> options = new List<CustomSenderService.PicklistOption>{
            new CustomSenderService.PicklistOption('testB', 'testB', 'testB', false, 'testB'),
            new CustomSenderService.PicklistOption('testA', 'testA', 'testA', false, 'testA')
        };
        options.sort();

        System.assertEquals('testA', options[0].name);
    }

    static smagicinteract__SMS_SenderId__c getNewSenderId() {
        return new smagicinteract__SMS_SenderId__c(
            smagicinteract__senderId__c = '16152377022',
            smagicinteract__Enable_Email_To_Text__c = false,
            smagicinteract__Configured_Manage_Notification__c = false,
            smagicinteract__Used_For__c = 'Both',
            smagicinteract__Email_Notification_Template__c = 'default',
            smagicinteract__Notification_Recipient__c = 'User assigned to Sender Id',
            smagicinteract__Channel_Code__c = '1',
            smagicinteract__Channel_Name__c = 'SMS'
        );
    }
}